package ru.t1.nikitushkina.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class ServerVersionRequest extends AbstractUserRequest {

    public ServerVersionRequest(@Nullable String token) {
        super(token);
    }

}
